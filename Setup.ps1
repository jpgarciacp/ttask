Write-Host "Cleaning artifacts"
rm dist -Force -ErrorAction Ignore
rm 	ttask.egg-info -Force -ErrorAction Ignore
rm ttask/__pycache__ -Force -ErrorAction Ignore
Write-Host "Building"
poetry update
poetry env use python
poetry build
Write-Host "Remove Old Version"
python -m pip uninstall ttask
Write-Host "Installing"
$file = Get-ChildItem -Path .\dist\ -Recurse -Include *.tar.gz
python -m pip install $file
Write-Host "Ok cool"
