# Cecropia's TimeTask Client
A command-line tool to register time onto Cecropia's timetask platform.

## Dependencies
- Python3(Python3.10 has some compatibility issues)
- python3-pip
- [poetry](https://github.com/python-poetry/poetry)

## Install (Only tested on Linux)
```bash
make install
```
## Install windows (needs to be run as Administrator)
if needed update inside the poweshell script the route of your python installation
ex:
```
poetry env use [python installation]
```
```
Setup.ps1
```

## First time use
- Login on [timetask](https://cecropia.timetask.com/)
- Get you API_KEY from: My Account -> API access -> Generate Token
- Execute and follow the instructions:

```bash
ttask
```

# Templates
Templates are a shortcut to your regular time inputs, you can create as many templates as you need. The script has Vacation and Holiday template pre-installed. You can list your current templates with:

```bash
ttask --templates
```

To create a template, you need to run and follow the instructions:

```bash
ttask tname #Where 'tname' is your shortcut
```

You will be asked for:
- Choosing the client
- Choosing the project (If the client only has one, it will choose it automatically)
- Choosing the module (If the project only has one, it will choose it automatically)
- Choosing the Worktype (If the project only has one, it will choose it automatically)
- Writing the default time: floating point
- Writing the default description, it will open your OS default editor
- Choosing if the time is billable by default.

You can change those default values on run-time by adding more arguments. You can find them running:

```bash
ttask -h
```

# Run
You just need to run:

```bash
ttask TEMPLATE_NAME
```

Your default editor will be opened, and after saving and closing the file, the time will be added to your account.

If you need to add time for a previous or future time run:

```bash
ttask TEMPLATE_NAME --date=YYYY-MM-DD
```
Templates Folder:
`cd ~\.config\ttask\templates`