# A Makefile for commands I run frequently:

clean:
	rm -rf dist
	rm -rf ttask.egg-info
	rm -rf ttask/__pycache__

build: clean
	poetry build

install: clean build
	rm dist/*.whl
	pip3.9 install dist/ttask-*
