#!/usr/bin/env python
import base64
from python_http_client import Client

URL_BASE = "https://api.myintervals.com/"


def create_api_client(config):
    if config is not None and 'api_key' in config.keys():
        API_KEY_ENCODED = str(base64.b64encode(
            ("%s:X" % config['api_key']).encode("utf-8")), "utf-8")
        GLOBAL_HEADERS = {"Authorization": (
            "Basic %s" % API_KEY_ENCODED), "Accept": "application/json"}
        return Client(host=URL_BASE, request_headers=GLOBAL_HEADERS)
    else:
        print('api_key not found on config')
        return None
