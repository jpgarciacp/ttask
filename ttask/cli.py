from __future__ import print_function, unicode_literals
from PyInquirer import prompt
import json
import argparse
from datetime import datetime, timedelta
from pathlib import Path
import os.path
from sys import platform
import yaml
from . import util
from . import api
from . import install
from termgraph import termgraph as tg


def parse_args():
    dt = datetime.now()
    parser = argparse.ArgumentParser()
    parser.add_argument('template', nargs='?')
    parser.add_argument(
        '-d', '--date', type=lambda s: datetime.strptime(s, '%Y-%m-%d'), dest='date',
        default=datetime.now().strftime('%Y-%m-%d'))
    parser.add_argument('-t', '--time', type=float, dest='time', help='Time')
    parser.add_argument('-b', '--billable', dest='billable', help='Billable',
                        type=lambda x: (str(x).lower() in ['true', '1', 'yes']))
    parser.add_argument('--templates', action='store_true', dest='templates', help='List the templates', default=False)
    parser.add_argument('--summary', action='store_true', dest='summary', help='', default=False)
    parser.add_argument('-m', '--month', type=int, dest='month', help='Month', default=dt.month)
    parser.add_argument('-y', '--year', type=int, dest='year', help='Year', default=dt.year)
    parser.add_argument('-e', '--edit', action='store_true', dest='edit', help='Force edit template', default=False)
    args = parser.parse_args()
    if args.template is None and args.templates is False and args.summary is False:
        print('You need to specify a template')
        parser.print_help()
    return args


def run():
    args = parse_args()
    config = install.load_or_install()
    client = api.create_api_client(config)
    template_name = args.template
    args.template = install.load_template(template_name)
    if args.template is None:
        if args.templates:
            install.print_templates()
        elif args.summary:
            print_summary(config, client, args.month, args.year)
        elif template_name:
            ask_template_creation(config, template_name)
    else:
        save_time(config, client, args)


def print_summary(config, client, month, year):
    dt = datetime.now()
    start = dt.replace(day=1, month=month, year=year)
    end = last_day_of_month(start)
    date_range = get_date_range(start, end)
    print('\nMonthly Work: %s' % start.strftime('%B - %Y\n--'))
    params = {
        'personid': config['personid'],
        'datebegin': start.strftime('%Y-%m-%d'),
        'dateend': end.strftime('%Y-%m-%d'),
        'limit': 20*31
        }
    response = client._('time').get(query_params=params)
    time_data = json.loads(response.body)['time']
    colors = [94, 31]
    args = {'filename': 'data/ex1.dat', 'title': 'Work week', 'width': 50,
            'format': '{:<5.2f}', 'suffix': '', 'no_labels': False,
            'color': None, 'vertical': False, 'stacked': True,
            'different_scale': False, 'calendar': False,
            'start_dt': None, 'custom_tick': '', 'delim': '',
            'verbose': False, 'version': False}
    labels = []
    data = []
    week_billable = 0
    week_nbillable = 0
    for dt in date_range:
        labels.append(dt.strftime('%a %d'))
        b_data = [float(item['time']) for item in time_data if item['dateiso'] == dt.strftime('%Y-%m-%d') and item['billable'] == 't']
        b_num = sum(b_data) if len(b_data) > 0 else float(0)
        nb_data = [float(item['time']) for item in time_data if item['dateiso'] == dt.strftime('%Y-%m-%d') and item['billable'] == 'f']
        nb_num = sum(nb_data) if len(nb_data) > 0 else float(0)
        data.append([b_num, nb_num])
        week_billable = week_billable + b_num
        week_nbillable = week_nbillable + nb_num
        if dt.weekday() == 5:
            tg.chart(colors, data, args, labels)
            labels = []
            data = []
            print('Total: %0.2f hours (%0.2f billable, %0.2f non-billable)' % (week_billable + week_nbillable, week_billable, week_nbillable))
            print('\n')
            week_billable = 0
            week_nbillable = 0
    if len(data) > 0:
        tg.chart(colors, data, args, labels)
        print('Total: %0.2f hours (%0.2f billable, %0.2f non-billable)' % (week_billable + week_nbillable, week_billable, week_nbillable))


def get_date_range(start, end):
    result = [start]
    num = 1
    while True:
        d = start + timedelta(days=num)
        num += 1
        if d <= end:
            result.append(d)
        else:
            break
    return result


def last_day_of_month(any_day):
    next_month = any_day.replace(day=28) + timedelta(days=4)
    return next_month - timedelta(days=next_month.day)


def ask_template_creation(config, template_name):
    answers = prompt([{
        'type': 'confirm',
        'name': 'confirm',
        'message': 'The template "%s" does not exist. Would you like to create it? (y/n): ' % template_name
    }])
    if answers['confirm']:
        install.create_template(config, template_name)


def save_time(config, client, args):
    data = args.template
    data['personid'] = config['personid']
    if(args.time is not None):
        data['time'] = args.time
    if(args.billable is not None):
        data['billable'] = 't' if args.billable else 'f'
    data['date'] = args.date.strftime('%Y-%m-%d')

    if platform != 'darwin':
        if 'gitmacro' in data['meta'].keys():
            data['description'] += util.get_git_log(
                data['meta']['gitmacro'], args.date)
    else:
        print('Git macro not implemented in MacOS')

    data['description'] = util.get_text_from_editor(
        config, data['description']) if data['meta']['edit'] or args.edit else data['description']

    confirm = True
    if 'meta' in data:
        if 'confirm' in data['meta']:
            confirm = data['meta']['confirm']
    del data['meta']
    save = util.confirm('Are you sure you want to save %s?' % util.formatdir(data)) if confirm else True

    if save:
        client._('time').post(request_body=data)
        print_summary(config,client,args.month, args.year)
    else:
        print('Task was not saved')
