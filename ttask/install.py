#!/usr/bin/env python

from __future__ import print_function, unicode_literals
from PyInquirer import prompt
import xdg.BaseDirectory
import os
import logging
import sys
import json
import yaml
import glob
import copy
from . import api
from . import util

if "win32" not in sys.platform:
    # readline is not included in Windows Active Python
    import readline

XDG_RESOURCE = 'ttask'
DEFAULT_CONFIG_NAME = 'ttask.yaml'
DEFAULT_TEMPLATE_PATH = 'templates'

USER_HOME = os.path.expanduser('~')
CONFIG_PATH = xdg.BaseDirectory.save_config_path(XDG_RESOURCE) or USER_HOME
CONFIG_FILE_PATH = os.path.join(CONFIG_PATH, DEFAULT_CONFIG_NAME)
TEMPLATES_FILE_PATH = os.path.join(CONFIG_PATH, DEFAULT_TEMPLATE_PATH)
CONFIG_FILE_PATH_FALLBACK = os.path.join(USER_HOME, ".ttask")

log = logging.getLogger(__name__)

default_config = {
    'api_key': None,
    'editor': 'vim',
    'personid': None
}

default_template = {
    'taskid': 0,
    'personid': 0,
    'projectid': 0,
    'moduleid': 0,
    'worktypeid': 0,
    'time': 8,
    'description': 'Vacation day.',
    'billable': 'f',
    'date': None,
    'meta': {
        'edit': True
    }
}

default_templates = {
    'vacation': {
        'taskid': 0,
        'personid': 0,
        'projectid': 157388,
        'moduleid': 138968,
        'worktypeid': 216390,
        'time': 8,
        'description': 'Vacation day.',
        'billable': 'f',
        'date': None,
        'meta': {
            'edit': False
        }
    },
    'holiday': {
        'taskid': 0,
        'personid': 0,
        'projectid': 157388,
        'moduleid': 150117,
        'worktypeid': 216390,
        'time': 8,
        'description': '',
        'billable': 'f',
        'date': None,
        'meta': {
            'edit': False
        }
    },
    'training': {
        'taskid': 0,
        'personid': 0,
        'projectid': 157388,
        'moduleid': 150117,
        'worktypeid': 216390,
        'time': 1,
        'description': '',
        'billable': 'f',
        'date': None,
        'meta': {
            'confirm': True,
            'edit': True
        }
    },
    'incapacity': {
        'billable': False,
        'date': None,
        'description': 'Incapacity day',
        'meta': {
            'confirm': True,
            'edit': False
        },
        'moduleid': 287999,
        'personid': 0,
        'projectid': 157388,
        'taskid': 0,
        'time': 8.0,
        'worktypeid': 216390
    }
}


def save_config(config):
    with open(CONFIG_FILE_PATH, 'w') as f:
        yaml.safe_dump(config, f, encoding='utf-8',
                       allow_unicode=True, default_flow_style=False)


def load_template(template):
    try:
        return util.load_yaml(os.path.join(TEMPLATES_FILE_PATH, '%s.yaml' % template))
    except:
        return None


def print_templates():
    files = [f for f in os.listdir(TEMPLATES_FILE_PATH) if os.path.isfile(os.path.join(TEMPLATES_FILE_PATH, f))]
    for f in files:
        try:
            print('\n---- %s' % f)
            util.dumpdir(util.load_yaml(os.path.join(TEMPLATES_FILE_PATH, f)))
        except:
            return None


def load_or_install():
    config_path = CONFIG_FILE_PATH if os.path.exists(
        CONFIG_FILE_PATH) else CONFIG_FILE_PATH_FALLBACK
    if os.path.exists(config_path):
        log.debug('Reading configuration from file %s', config_path)
        return util.load_yaml(config_path)
    else:
        log.debug('Configuration file not found, installing jrnl...')
        try:
            config = install()
        except KeyboardInterrupt:
            raise Exception("Installation aborted")
        return config


def install():
    if "win32" not in sys.platform:
        readline.set_completer_delims(' \t\n;')
        readline.parse_and_bind("tab: complete")
        readline.set_completer(autocomplete)

    path_query = f'Timetask api_key: '
    api_key = input(path_query).strip() or None
    default_config['api_key'] = os.path.expanduser(os.path.expandvars(api_key))

    path_query = f'Editor (vim): '
    editor = input(path_query).strip() or "vim"
    default_config['editor'] = os.path.expanduser(os.path.expandvars(editor))

    log.info("Getting the personid from API")
    client = api.create_api_client(default_config)
    response = client._('me').get()
    personid = json.loads(response.body)['personid']
    if personid:
        print(personid)
        default_config['personid'] = personid
    else:
        print("PersonId could not be found")
    copy_templates()
    save_config(default_config)
    return default_config


def copy_templates():
    for key, value in default_templates.items():
        filename = os.path.join(TEMPLATES_FILE_PATH, '%s.yaml' % key)
        if not os.path.exists(TEMPLATES_FILE_PATH):
            os.makedirs(TEMPLATES_FILE_PATH)
        with open(filename, 'w') as f:
            yaml.safe_dump(value, f, encoding='utf-8',
                           allow_unicode=True, default_flow_style=False)


def autocomplete(text, state):
    expansions = glob.glob(os.path.expanduser(os.path.expandvars(text)) + '*')
    expansions = [e + "/" if os.path.isdir(e) else e for e in expansions]
    expansions.append(None)
    return expansions[state]


def choose_resource(client, resource_name, id_key, params={}):
    response = client._(resource_name).get(query_params=params)
    resources = json.loads(response.body)[resource_name]
    if len(resources) > 1:
        name = 'worktype' if 'worktype' in resource_name else 'modulename' if 'projectmodule' in resource_name else 'name'
        questions = [
            {
                'name': resource_name,
                'message': f'Choose a {resource_name}',
                'type': 'list',
                'choices': [f'{c[id_key]:6}: {c[name]}' for c in resources]
            }
        ]
        answers = prompt(questions)
        return int(answers[resource_name].split(':')[0].strip())
    elif len(resources) == 1:
        return int(resources[0][id_key])
    return None


def create_template(config, template_name):
    client = api.create_api_client(config)
    template = copy.copy(default_template)
    clientid = choose_resource(client, 'client', 'id')
    template['projectid'] = choose_resource(client, 'project', 'id', {'clientid': clientid, 'limit': 100})
    template['moduleid'] = choose_resource(client, 'projectmodule', 'moduleid', {
                               'projectid': template['projectid'], 'personid': config['personid'], 'limit': 100})
    template['worktypeid'] = choose_resource(client, 'projectworktype', 'worktypeid', {
                                 'projectid': template['projectid'], 'limit': 100})
    questions = [
        {'name': 'time', 'type': 'input', 'message': 'Default time: '},
        {'name': 'description', 'type': 'input','multiline':True, 'message': 'Default description (close:Ctrl-O): '},
        {'name': 'billable', 'type': 'confirm', 'message': 'Billable?: '},
        {'name': 'edit', 'type': 'confirm', 'message': 'Would you like to edit the description each time you use it?: '},
        {'name': 'confirm', 'type': 'confirm', 'message': 'Would you like to confirm before save?'}
        ]
    answers = prompt(questions)
    template['time'] = float(answers['time'])
    template['description'] = answers['description']
    template['billable'] = 't' if answers['billable'] else 'f'
    template['meta']['edit'] = answers['edit']
    template['meta']['confirm'] = answers['confirm']
    filename = os.path.join(TEMPLATES_FILE_PATH, '%s.yaml' % template_name)
    with open(filename, 'w') as f:
        yaml.safe_dump(template, f, encoding='utf-8',
                       allow_unicode=True, default_flow_style=False)
    print(f'Template "{filename}" created. Use it as: ttask {template_name}')
