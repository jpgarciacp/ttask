import tempfile
import subprocess
import shlex
import sys
import os
import json
import yaml
from PyInquirer import prompt, print_json


def confirm(question):
    questions = [
        {'name': 'confirm', 'type': 'confirm', 'message': question}
        ]
    answers = prompt(questions)
    return answers['confirm']

def get_text_from_editor(config, template=''):
    filehandle, tmpfile = tempfile.mkstemp(
        prefix="jp", text=True, suffix=".txt")
    with open(tmpfile, 'w', encoding="utf-8") as f:
        if template:
            f.write(template)
    try:
        subprocess.call(shlex.split(
            config['editor'], posix="win" not in sys.platform) + [tmpfile])
    except AttributeError:
        subprocess.call(config['editor'] + [tmpfile])
    with open(tmpfile, "r", encoding="utf-8") as f:
        raw = f.read()
    os.close(filehandle)
    os.remove(tmpfile)
    if not raw:
        print('[Nothing saved to file]', file=sys.stderr)
    return raw


def get_git_log(macro, date):
    raw = ""
    repos = macro['repos']
    for item in repos.keys():
        raw += subprocess.check_output(['git', '--git-dir', '%s/.git' % repos[item],
                                        'log', '--pretty=format:"[%s] %%s"' % item, '--after',
                                        '%s 00:00:00' % date, '--before', '%s 23:59:59' % date,
                                        '--date', 'short', '--author', macro['author']]).decode()
    return raw.replace('"', '')


def load_yaml(config_path):
    """Tries to load a config file from YAML.
    """
    with open(config_path) as f:
        return yaml.load(f, Loader=yaml.FullLoader)


def dumparr(arr):
    for item in arr:
        dumpdir(item)
        print("\n")


def dumpdir(item):
    for key, val in item.items():
        if isinstance(val, dict):
            val = json.dumps(val)
        print("{0:15}: {1:50}".format(
            key, str(val) if val is not None else "None"))

def formatdir(item):
    text = ""
    for key, val in item.items():
        if isinstance(val, dict):
            val = json.dumps(val)
        text += "\n{0:15}: {1:50}".format(
            key, str(val) if val is not None else "None")
    return text
